import React, { Component } from 'react';
import './App.css';


// Fucboy enabler
import { Route, Switch, } from "react-router-dom";

// MGA BITCH MO
import Angela from './components/Angela'  // MAIN BITCH
import Bea from './components/Bea' // Side Hoe
import Lexine from './components/Lexine' // Side Hoe


/*
Routes are defined here and all your routing is already wrapped
*/

class App extends Component {
  render() {

    return (
      <div>
        {/*
        <Switch>
        <Route exact path="/" component={Login} />
        <Route path="/view" component={Viewer} />
        <Route path="/approvals" component={ Private } />
        <Route path="/approved" component={ Approved } />
        <Route path="/search" component={ Search } />
        </Switch>
        */}
       
        <h1> Lesson 3: Routing </h1>
        <marquee scrollamount="1000">
        <div className="flex-container">
        <a href="/bea"> Bea </a>  
        <a href="/lexine"> Lexine</a>
        <a href="/"> Angela </a>
        </div>
        </marquee>
        <Switch>
          <Route exact path="/" component={Angela} />
          <Route path="/bea" component={ Bea } />
          <Route path="/lexine" component={ Lexine} />
        </Switch>

      {/* Flexbox tutorial 
        <div className="flex-container">
          <div className="red"> 1 </div>
          <div className="blue"> 2 </div>
          <div className="red"> 3 </div>
          <div className="blue"> 4 </div> 
        </div>
      */}
      </div>
    );
  }
}

export default App;

// https://css-tricks.com/snippets/css/a-guide-to-flexbox/